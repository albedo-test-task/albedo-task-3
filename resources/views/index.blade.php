@extends('layouts.app')

@section('content')
    <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3303.7615498731234!2d-118.34587228544304!3d34.10124852259468!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c2bf20e4c82873%3A0x14015754d926dadb!2zNzA2MCBIb2xseXdvb2QgQmx2ZCwgTG9zIEFuZ2VsZXMsIENBIDkwMDI4LCDQodCo0JA!5e0!3m2!1sru!2sua!4v1655853601475!5m2!1sru!2sua"
        height="450" style="border:0; width:100%" allowfullscreen="" loading="lazy"
        referrerpolicy="no-referrer-when-downgrade"></iframe>

    <div class="container" id="form-block">
        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-8">
                <div id="app">
                    <form id="registration" name="registration" @submit="sentData">
                        <nav>
                            <div class="nav nav-pills nav-fill" id="nav-tab" role="tablist">
                                <div class="nav-link active" id="step1-tab" href="#step1">Step 1</div>
                                <div class="nav-link" id="step2-tab" href="#step2">Step 2</div>
                            </div>
                        </nav>

                        <div class="tab-content py-4">
                            <div id="step1">
                                @include('member-form.step1')
                            </div>
                            <div id="step2" class="d-none">
                                @include('member-form.step2')
                            </div>
                        </div>

                        <div class="row justify-content-between">
                            <div class="col-auto">
                                <button type="button" class="btn btn-secondary" id="previous"
                                        v-if="step != 1"
                                        @click="previousClick"
                                        data-enchanter="previous">
                                    Previous
                                </button>
                            </div>
                            <div class="col-auto">
                                <button type="button" class="btn btn-primary" data-enchanter="next" id="next"
                                        v-if="step == 1"
                                        @click="nextClick"
                                >Next
                                </button>
                                <button type="submit" class="btn btn-primary" data-enchanter="finish"
                                        v-if="step == 2"
                                        id="finish">Finish
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row py-4 d-none" id="after-form-block">
        <div class="col">
            <div class="text-center">
                <ul class="social-icons">
                    <li><a class="social-icon-twitter" href="https://twitter.com/test_tw"
                           title="Check out this Meetup with SoCal AngularJS!" target="_blank" rel="noopener"></a></li>
                    <li><a class="social-icon-fb" href="https://www.facebook.com/test_fb"
                           title="Check out this Meetup with SoCal AngularJS!" target="_blank" rel="noopener"></a></li>
                </ul>

                <h3 style="text-align: center; margin-bottom:50px;"><a class="nav-link" href="/members">View the list of
                        participants</a></h3>
            </div>
        </div>
    </div>
@endsection
